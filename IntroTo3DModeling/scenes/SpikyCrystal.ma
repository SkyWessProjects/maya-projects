//Maya ASCII 2022 scene
//Name: SpikyCrystal.ma
//Last modified: Sat, Aug 28, 2021 12:24:26 AM
//Codeset: 1252
requires maya "2022";
requires "mtoa" "4.2.1";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2022";
fileInfo "version" "2022";
fileInfo "cutIdentifier" "202102181415-29bfc1879c";
fileInfo "osv" "Windows 10 Home v2009 (Build: 19043)";
fileInfo "UUID" "384BD1C4-44D6-CD4D-51BE-95A81E7FC1AB";
createNode transform -s -n "persp";
	rename -uid "E8FD22CE-4B01-2480-EC09-0A882B323895";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -41.186586026755137 17.180302173256589 4.1835808462578292 ;
	setAttr ".r" -type "double3" 337.46164726852726 -1164.1999999995792 -1.573654868570733e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "6CF6B72E-4F31-40B7-69A1-F483DF9D59CE";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 44.821869662031688;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "C1663FA1-4B27-3AA9-3714-47871CD116E4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "8153EDFD-41F8-A438-1DCE-F8892F835F66";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "1D48467D-4DB0-F00B-1CF1-5E9E6470DF73";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "67615158-4C55-7CAD-F51A-53BA6D1153AC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "387BCEA0-4136-51CA-658B-D9B7D592B876";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "787FEBA7-434B-803F-4349-D9A5307F43B4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pSphere7";
	rename -uid "1A52B115-433E-20CC-2ECA-59A061E0A25C";
	setAttr ".rp" -type "double3" -0.19167962192671451 3.0128836151040534 -0.25362008652349455 ;
	setAttr ".sp" -type "double3" -0.19167962192671451 3.0128836151040534 -0.25362008652349455 ;
createNode mesh -n "pSphere7Shape" -p "pSphere7";
	rename -uid "F241130A-479F-77C8-CB6A-4EAE52888390";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:95]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 192 ".uvst[0].uvsp[0:191]" -type "float2" 0.075000003 0 0.125
		 0 0.375 0 0.52499998 0 0.625 0 0.72499996 0 0.82499999 0 0.97499996 0 0.075000003
		 1 0.125 1 0.32500002 1 0.375 1 0.52499998 1 0.625 1 0.72499996 1 0.82499999 1 0.97499996
		 1 0.4438594 0.52806193 0.54366809 0.5227055 0.59349692 0.51791257 0.64329898 0.51236928
		 0.65670121 0.51236928 0.74361151 0.52112067 0.7563886 0.52112067 0.84361726 0.52127957
		 0.893498 0.51794058 0.99308467 0.50636888 0.0069153993 0.50636888 0.093260601 0.51129687
		 0.1067394 0.51129687 0.14321917 0.51013696 0.19309472 0.5066517 0.0069153993 0.50636888
		 0.1067394 0.51129687 0.075000003 1 0.19309472 0.5066517 0.125 1 0.375 0 0.32500002
		 1 0.4438594 0.52806193 0.375 1 0.59349692 0.51791257 0.52499998 1 0.65670121 0.51236928
		 0.625 1 0.7563886 0.52112067 0.72499996 1 0.893498 0.51794058 0.82499999 1 0.99308467
		 0.50636888 0.97499996 1 0.54366809 0.5227055 0.52499998 0 0.64329898 0.51236928 0.625
		 0 0.74361151 0.52112067 0.72499996 0 0.84361726 0.52127957 0.82499999 0 0.97499996
		 0 0.093260601 0.51129687 0.075000003 0 0.14321917 0.51013696 0.125 0 0.0069153993
		 0.50636888 0.1067394 0.51129687 0.075000003 1 0.19309472 0.5066517 0.125 1 0.375
		 0 0.32500002 1 0.4438594 0.52806193 0.375 1 0.59349692 0.51791257 0.52499998 1 0.65670121
		 0.51236928 0.625 1 0.7563886 0.52112067 0.72499996 1 0.893498 0.51794058 0.82499999
		 1 0.99308467 0.50636888 0.97499996 1 0.54366809 0.5227055 0.52499998 0 0.64329898
		 0.51236928 0.625 0 0.74361151 0.52112067 0.72499996 0 0.84361726 0.52127957 0.82499999
		 0 0.97499996 0 0.093260601 0.51129687 0.075000003 0 0.14321917 0.51013696 0.125 0
		 0.0069153993 0.50636888 0.1067394 0.51129687 0.075000003 1 0.19309472 0.5066517 0.125
		 1 0.375 0 0.32500002 1 0.4438594 0.52806193 0.375 1 0.59349692 0.51791257 0.52499998
		 1 0.65670121 0.51236928 0.625 1 0.7563886 0.52112067 0.72499996 1 0.893498 0.51794058
		 0.82499999 1 0.99308467 0.50636888 0.97499996 1 0.54366809 0.5227055 0.52499998 0
		 0.64329898 0.51236928 0.625 0 0.74361151 0.52112067 0.72499996 0 0.84361726 0.52127957
		 0.82499999 0 0.97499996 0 0.093260601 0.51129687 0.075000003 0 0.14321917 0.51013696
		 0.125 0 0.0069153993 0.50636888 0.1067394 0.51129687 0.075000003 1 0.19309472 0.5066517
		 0.125 1 0.375 0 0.32500002 1 0.4438594 0.52806193 0.375 1 0.59349692 0.51791257 0.52499998
		 1 0.65670121 0.51236928 0.625 1 0.7563886 0.52112067 0.72499996 1 0.893498 0.51794058
		 0.82499999 1 0.99308467 0.50636888 0.97499996 1 0.54366809 0.5227055 0.52499998 0
		 0.64329898 0.51236928 0.625 0 0.74361151 0.52112067 0.72499996 0 0.84361726 0.52127957
		 0.82499999 0 0.97499996 0 0.093260601 0.51129687 0.075000003 0 0.14321917 0.51013696
		 0.125 0 0.0069153993 0.50636888 0.1067394 0.51129687 0.075000003 1 0.19309472 0.5066517
		 0.125 1 0.375 0 0.32500002 1 0.4438594 0.52806193 0.375 1 0.59349692 0.51791257 0.52499998
		 1 0.65670121 0.51236928 0.625 1 0.7563886 0.52112067 0.72499996 1 0.893498 0.51794058
		 0.82499999 1 0.99308467 0.50636888 0.97499996 1 0.54366809 0.5227055 0.52499998 0
		 0.64329898 0.51236928 0.625 0 0.74361151 0.52112067 0.72499996 0 0.84361726 0.52127957
		 0.82499999 0 0.97499996 0 0.093260601 0.51129687 0.075000003 0 0.14321917 0.51013696
		 0.125 0;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 60 ".vt[0:59]"  -0.10754681 0.1381408 -0.043291211 3.14667988 3.89044714 -2.061663628
		 0.53936428 1.83789289 -0.69562036 0.88454664 1.77956939 -0.24750781 1.037218571 1.6821754 -0.18241692
		 1.3210026 1.42857242 -0.19633985 1.49360585 1.06593132 -0.59222782 1.37172627 0.98338282 -0.94219601
		 1.1213572 1.054280758 -1.21406221 0.82411724 1.26834345 -1.29534459 -0.027682543 0.027421236 -0.31495452
		 3.025486469 3.89384198 2.52552009 0.38601017 1.38305545 1.2767607 0.95062894 0.89370215 1.33596373
		 1.17328894 0.80661261 1.21517599 1.55973065 0.77583635 0.84169114 1.7159903 1.13359869 0.18674833
		 1.4776392 1.49271429 -0.04587853 1.07891798 1.79599416 -0.030121922 0.65465808 1.92138886 0.25522065
		 0 -0.023326159 0 0 6.65480614 0 -0.96351683 2.25351119 -0.0037505648 -0.55325806 2.25351048 0.76918221
		 -0.28526154 2.25351024 0.89516115 0.30060112 2.25351095 0.91051483 0.90882504 2.25351048 0.29149938
		 0.88863683 2.25350952 -0.29240429 0.55650353 2.25350952 -0.76681048 0.0051893592 2.25350976 -0.93837541
		 0.16509631 0.19391942 -0.07807982 -0.9906112 4.082043648 -3.64896011 -1.11131811 1.28975725 -1.2601403
		 -0.83183587 1.8503387 -0.74020839 -0.60199189 1.98920178 -0.66339672 -0.067090273 2.13675976 -0.67584991
		 0.56744552 1.90902293 -1.1291852 0.62179613 1.55395579 -1.53338623 0.37665471 1.19091654 -1.84933925
		 -0.10710168 0.95780206 -1.94659758 0.071412086 0.23381817 -0.18781166 -3.7199533 3.59623814 -0.5272761
		 -1.69790184 0.87464154 0.012844503 -1.36807179 1.28291607 0.37308162 -1.2149359 1.45479667 0.36524826
		 -0.92293751 1.76547062 0.18125558 -0.72422242 1.92911637 -0.41720119 -0.83022928 1.77243197 -0.78520393
		 -1.072359562 1.479774 -0.97973019 -1.3729794 1.14810562 -0.90740716 -0.05593574 0.15505052 -0.37504947
		 -1.52640557 3.70694804 2.94659233 -1.071795106 0.793805 1.14154816 -0.33595562 0.85871315 1.39789081
		 -0.10914391 0.98892337 1.35906243 0.21246535 1.33383465 1.13261724 0.12095821 1.83904791 0.55187255
		 -0.28095907 1.96234739 0.24209857 -0.77528346 1.87472987 0.11695552 -1.18318868 1.58656716 0.24451661;
	setAttr -s 144 ".ed[0:143]"  7 8 1 8 9 1 0 2 1 2 3 1 3 4 1 4 5 1 5 6 1
		 6 7 1 0 7 1 0 8 1 0 9 1 0 3 1 0 4 1 0 5 1 0 6 1 7 1 1 8 1 1 9 1 1 0 1 1 2 1 1 3 1 1
		 4 1 1 5 1 1 6 1 1 17 18 1 18 19 1 10 12 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1
		 10 17 1 10 18 1 10 19 1 10 13 1 10 14 1 10 15 1 10 16 1 17 11 1 18 11 1 19 11 1 10 11 1
		 12 11 1 13 11 1 14 11 1 15 11 1 16 11 1 27 28 1 28 29 1 20 22 1 22 23 1 23 24 1 24 25 1
		 25 26 1 26 27 1 20 27 1 20 28 1 20 29 1 20 23 1 20 24 1 20 25 1 20 26 1 27 21 1 28 21 1
		 29 21 1 20 21 1 22 21 1 23 21 1 24 21 1 25 21 1 26 21 1 37 38 1 38 39 1 30 32 1 32 33 1
		 33 34 1 34 35 1 35 36 1 36 37 1 30 37 1 30 38 1 30 39 1 30 33 1 30 34 1 30 35 1 30 36 1
		 37 31 1 38 31 1 39 31 1 30 31 1 32 31 1 33 31 1 34 31 1 35 31 1 36 31 1 47 48 1 48 49 1
		 40 42 1 42 43 1 43 44 1 44 45 1 45 46 1 46 47 1 40 47 1 40 48 1 40 49 1 40 43 1 40 44 1
		 40 45 1 40 46 1 47 41 1 48 41 1 49 41 1 40 41 1 42 41 1 43 41 1 44 41 1 45 41 1 46 41 1
		 57 58 1 58 59 1 50 52 1 52 53 1 53 54 1 54 55 1 55 56 1 56 57 1 50 57 1 50 58 1 50 59 1
		 50 53 1 50 54 1 50 55 1 50 56 1 57 51 1 58 51 1 59 51 1 50 51 1 52 51 1 53 51 1 54 51 1
		 55 51 1 56 51 1;
	setAttr -s 96 -ch 288 ".fc[0:95]" -type "polyFaces" 
		f 3 0 16 -16
		mu 0 3 27 29 8
		f 3 1 17 -17
		mu 0 3 29 31 9
		f 3 -11 18 -18
		mu 0 3 31 2 10
		f 3 2 19 -19
		mu 0 3 2 17 11
		f 3 3 20 -20
		mu 0 3 17 19 12
		f 3 4 21 -21
		mu 0 3 19 21 13
		f 3 5 22 -22
		mu 0 3 21 23 14
		f 3 6 23 -23
		mu 0 3 23 25 15
		f 3 7 15 -24
		mu 0 3 25 26 16
		f 3 -4 -3 11
		mu 0 3 18 17 3
		f 3 -5 -12 12
		mu 0 3 20 19 4
		f 3 -6 -13 13
		mu 0 3 22 21 5
		f 3 -7 -14 14
		mu 0 3 24 23 6
		f 3 -8 -15 8
		mu 0 3 26 25 7
		f 3 -1 -9 9
		mu 0 3 28 27 0
		f 3 -2 -10 10
		mu 0 3 30 29 1
		f 3 24 40 -40
		mu 0 3 32 33 34
		f 3 25 41 -41
		mu 0 3 33 35 36
		f 3 -35 42 -42
		mu 0 3 35 37 38
		f 3 26 43 -43
		mu 0 3 37 39 40
		f 3 27 44 -44
		mu 0 3 39 41 42
		f 3 28 45 -45
		mu 0 3 41 43 44
		f 3 29 46 -46
		mu 0 3 43 45 46
		f 3 30 47 -47
		mu 0 3 45 47 48
		f 3 31 39 -48
		mu 0 3 47 49 50
		f 3 -28 -27 35
		mu 0 3 51 39 52
		f 3 -29 -36 36
		mu 0 3 53 41 54
		f 3 -30 -37 37
		mu 0 3 55 43 56
		f 3 -31 -38 38
		mu 0 3 57 45 58
		f 3 -32 -39 32
		mu 0 3 49 47 59
		f 3 -25 -33 33
		mu 0 3 60 32 61
		f 3 -26 -34 34
		mu 0 3 62 33 63
		f 3 48 64 -64
		mu 0 3 64 65 66
		f 3 49 65 -65
		mu 0 3 65 67 68
		f 3 -59 66 -66
		mu 0 3 67 69 70
		f 3 50 67 -67
		mu 0 3 69 71 72
		f 3 51 68 -68
		mu 0 3 71 73 74
		f 3 52 69 -69
		mu 0 3 73 75 76
		f 3 53 70 -70
		mu 0 3 75 77 78
		f 3 54 71 -71
		mu 0 3 77 79 80
		f 3 55 63 -72
		mu 0 3 79 81 82
		f 3 -52 -51 59
		mu 0 3 83 71 84
		f 3 -53 -60 60
		mu 0 3 85 73 86
		f 3 -54 -61 61
		mu 0 3 87 75 88
		f 3 -55 -62 62
		mu 0 3 89 77 90
		f 3 -56 -63 56
		mu 0 3 81 79 91
		f 3 -49 -57 57
		mu 0 3 92 64 93
		f 3 -50 -58 58
		mu 0 3 94 65 95
		f 3 72 88 -88
		mu 0 3 96 97 98
		f 3 73 89 -89
		mu 0 3 97 99 100
		f 3 -83 90 -90
		mu 0 3 99 101 102
		f 3 74 91 -91
		mu 0 3 101 103 104
		f 3 75 92 -92
		mu 0 3 103 105 106
		f 3 76 93 -93
		mu 0 3 105 107 108
		f 3 77 94 -94
		mu 0 3 107 109 110
		f 3 78 95 -95
		mu 0 3 109 111 112
		f 3 79 87 -96
		mu 0 3 111 113 114
		f 3 -76 -75 83
		mu 0 3 115 103 116
		f 3 -77 -84 84
		mu 0 3 117 105 118
		f 3 -78 -85 85
		mu 0 3 119 107 120
		f 3 -79 -86 86
		mu 0 3 121 109 122
		f 3 -80 -87 80
		mu 0 3 113 111 123
		f 3 -73 -81 81
		mu 0 3 124 96 125
		f 3 -74 -82 82
		mu 0 3 126 97 127
		f 3 96 112 -112
		mu 0 3 128 129 130
		f 3 97 113 -113
		mu 0 3 129 131 132
		f 3 -107 114 -114
		mu 0 3 131 133 134
		f 3 98 115 -115
		mu 0 3 133 135 136
		f 3 99 116 -116
		mu 0 3 135 137 138
		f 3 100 117 -117
		mu 0 3 137 139 140
		f 3 101 118 -118
		mu 0 3 139 141 142
		f 3 102 119 -119
		mu 0 3 141 143 144
		f 3 103 111 -120
		mu 0 3 143 145 146
		f 3 -100 -99 107
		mu 0 3 147 135 148
		f 3 -101 -108 108
		mu 0 3 149 137 150
		f 3 -102 -109 109
		mu 0 3 151 139 152
		f 3 -103 -110 110
		mu 0 3 153 141 154
		f 3 -104 -111 104
		mu 0 3 145 143 155
		f 3 -97 -105 105
		mu 0 3 156 128 157
		f 3 -98 -106 106
		mu 0 3 158 129 159
		f 3 120 136 -136
		mu 0 3 160 161 162
		f 3 121 137 -137
		mu 0 3 161 163 164
		f 3 -131 138 -138
		mu 0 3 163 165 166
		f 3 122 139 -139
		mu 0 3 165 167 168
		f 3 123 140 -140
		mu 0 3 167 169 170
		f 3 124 141 -141
		mu 0 3 169 171 172
		f 3 125 142 -142
		mu 0 3 171 173 174
		f 3 126 143 -143
		mu 0 3 173 175 176
		f 3 127 135 -144
		mu 0 3 175 177 178
		f 3 -124 -123 131
		mu 0 3 179 167 180
		f 3 -125 -132 132
		mu 0 3 181 169 182
		f 3 -126 -133 133
		mu 0 3 183 171 184
		f 3 -127 -134 134
		mu 0 3 185 173 186
		f 3 -128 -135 128
		mu 0 3 177 175 187
		f 3 -121 -129 129
		mu 0 3 188 160 189
		f 3 -122 -130 130
		mu 0 3 190 161 191;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "pCube7";
	rename -uid "068DAF47-47D4-7B46-7C53-97A34D43B562";
	setAttr ".rp" -type "double3" -0.042808410659735063 0.27623509696862414 -0.27449702080383642 ;
	setAttr ".sp" -type "double3" -0.042808410659735063 0.27623509696862414 -0.27449702080383642 ;
createNode mesh -n "pCube7Shape" -p "pCube7";
	rename -uid "5A15B967-41CA-DBDF-79B3-22BC228B8385";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:95]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 6 ".gtag";
	setAttr ".gtag[0].gtagnm" -type "string" "back";
	setAttr ".gtag[0].gtagcmp" -type "componentList" 1 "f[2]";
	setAttr ".gtag[1].gtagnm" -type "string" "bottom";
	setAttr ".gtag[1].gtagcmp" -type "componentList" 1 "f[3]";
	setAttr ".gtag[2].gtagnm" -type "string" "front";
	setAttr ".gtag[2].gtagcmp" -type "componentList" 1 "f[0]";
	setAttr ".gtag[3].gtagnm" -type "string" "left";
	setAttr ".gtag[3].gtagcmp" -type "componentList" 1 "f[5]";
	setAttr ".gtag[4].gtagnm" -type "string" "right";
	setAttr ".gtag[4].gtagcmp" -type "componentList" 1 "f[4]";
	setAttr ".gtag[5].gtagnm" -type "string" "top";
	setAttr ".gtag[5].gtagcmp" -type "componentList" 1 "f[1]";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 174 ".uvst[0].uvsp[0:173]" -type "float2" 0.375 0 0.375 1 0.625
		 0 0.625 1 0.375 0.25 0.375 0.5 0.125 0.25 0.625 0.5 0.875 0.25 0.375 0.75 0.125 0
		 0.25 0.25 0.625 0.75 0.875 0 0.5 0.75 0.5 0 0.5 1 0.5 0.25 0.5 0.5 0.625 0.25 0.375
		 0.375 0.625 0.375 0.75 0.25 0.5 0.375 0.375 0.875 0.25 0 0.625 0.875 0.75 0 0.5 0.875
		 0.5 0 0.625 0 0.625 0.25 0.5 0.25 0.375 0 0.375 0.25 0.5 0.375 0.375 0.375 0.625
		 0.375 0.625 0.5 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75 0.625 0.75 0.5 0.875 0.375
		 0.875 0.625 0.875 0.625 1 0.5 1 0.375 1 0.75 0 0.875 0 0.875 0.25 0.75 0.25 0.25
		 0 0.25 0.25 0.125 0 0.125 0.25 0.5 0 0.625 0 0.625 0.25 0.5 0.25 0.375 0 0.375 0.25
		 0.5 0.375 0.375 0.375 0.625 0.375 0.625 0.5 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75
		 0.625 0.75 0.5 0.875 0.375 0.875 0.625 0.875 0.625 1 0.5 1 0.375 1 0.75 0 0.875 0
		 0.875 0.25 0.75 0.25 0.25 0 0.25 0.25 0.125 0 0.125 0.25 0.5 0 0.625 0 0.625 0.25
		 0.5 0.25 0.375 0 0.375 0.25 0.5 0.375 0.375 0.375 0.625 0.375 0.625 0.5 0.5 0.5 0.375
		 0.5 0.5 0.75 0.375 0.75 0.625 0.75 0.5 0.875 0.375 0.875 0.625 0.875 0.625 1 0.5
		 1 0.375 1 0.75 0 0.875 0 0.875 0.25 0.75 0.25 0.25 0 0.25 0.25 0.125 0 0.125 0.25
		 0.5 0 0.625 0 0.625 0.25 0.5 0.25 0.375 0 0.375 0.25 0.5 0.375 0.375 0.375 0.625
		 0.375 0.625 0.5 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75 0.625 0.75 0.5 0.875 0.375
		 0.875 0.625 0.875 0.625 1 0.5 1 0.375 1 0.75 0 0.875 0 0.875 0.25 0.75 0.25 0.25
		 0 0.25 0.25 0.125 0 0.125 0.25 0.5 0 0.625 0 0.625 0.25 0.5 0.25 0.375 0 0.375 0.25
		 0.5 0.375 0.375 0.375 0.625 0.375 0.625 0.5 0.5 0.5 0.375 0.5 0.5 0.75 0.375 0.75
		 0.625 0.75 0.5 0.875 0.375 0.875 0.625 0.875 0.625 1 0.5 1 0.375 1 0.75 0 0.875 0
		 0.875 0.25 0.75 0.25 0.25 0 0.25 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 108 ".vt[0:107]"  -0.80176938 0.63779116 0.57585239 -0.82639849 0.026515484 0.53345299
		 -0.80136657 0.59534168 1.18762076 0.93100464 0.5259552 1.18166554 0.90637553 -0.085320473 1.13926601
		 0.93060184 0.56840467 0.56989717 0.90597272 -0.042870998 0.52749777 1.14610004 0.25922394 0.46744573
		 -1.042002678 0.34686375 0.4749676 -1.041493893 0.29324675 1.2476728 1.14660883 0.20560694 1.24015093
		 0.067602754 0.68908548 0.49798334 0.036494583 -0.082997799 0.44442987 0.05195564 0.31284857 0.32990897
		 -0.82599556 -0.01593399 1.14522123 0.068111569 0.63546848 1.27068853 0.037003368 -0.1366148 1.21713519
		 0.052650496 0.23962259 1.38520956 0.1228416 0.63581848 -1.061300039 0.11346626 0.023426533 -1.092134833
		 -0.96284723 0.61641026 -0.34573865 -0.0088598728 0.52904415 1.099334002 -0.018235445 -0.083347797 1.068499088
		 1.076829195 0.54845238 0.38377261 1.067453861 -0.063939571 0.3529377 1.34042835 0.23331738 0.46431065
		 0.13547754 0.34366703 -1.36091435 -1.2358222 0.31915331 -0.45711172 -0.030870914 0.20880365 1.36811352
		 0.74387383 0.67523861 -0.4288286 0.73203206 -0.098254681 -0.46777511 0.98870993 0.29297495 -0.61357212
		 -0.97222281 0.0040183067 -0.37657356 -0.62742567 0.65072489 0.47497416 -0.63926744 -0.1227684 0.43602777
		 -0.88410378 0.25949574 0.62077117 -0.22827637 0.6582427 -0.86146998 -0.26335609 -0.029713631 -0.90914178
		 0.57644999 0.5649395 -0.10715914 1.33476985 0.58218384 -0.9140296 1.29969013 -0.10577297 -0.96170115
		 0.53004324 0.67548704 -1.66834044 0.49496353 -0.012469292 -1.716012 0.50639927 0.34604979 -1.89752483
		 -0.45141017 0.32426882 -0.8783927 0.56501448 0.20641994 0.074353456 1.52282393 0.22820091 -0.94477868
		 0.049648523 0.76962757 -1.35785234 0.005340457 -0.099308968 -1.41806507 -0.1583699 0.35670948 -1.56217885
		 0.54137027 -0.12301731 -0.15483069 1.066073298 0.6517787 -0.40510631 1.021765113 -0.21715784 -0.46531892
		 1.22978389 0.19576025 -0.26099229 0.42156368 0.62788868 0.12608123 0.39500803 0.10709858 0.089993238
		 0.97568101 0.56364155 0.64548326 1.4978435 0.57551575 0.089890003 1.47128773 0.054725647 0.053802013
		 0.94372612 0.63976288 -0.42951202 0.91717046 0.11897278 -0.46560001 0.92624527 0.38937998 -0.58895481
		 0.26671851 0.3743825 0.11279738 0.96660644 0.29323435 0.76883781 1.62613297 0.30823183 0.067085624
		 0.6132527 0.71077824 -0.2152878 0.57971102 0.052984238 -0.26086944 0.4684996 0.39671993 -0.35804278
		 0.94912535 0.042851448 0.60939527 1.31314039 0.62963009 0.44075263 1.27959895 -0.02816391 0.39517105
		 1.42435193 0.28589439 0.53792584 -1.23873091 0.59667301 0.69332469 -1.27056503 -0.027637005 0.6500634
		 -0.236938 0.55920076 0.49691284 -0.43012762 0.63837862 -0.5035497 -0.46196175 0.014068604 -0.5468111
		 -1.43192053 0.67585087 -0.30713785 -1.46375465 0.051540852 -0.35039914 -1.60501862 0.37903929 -0.43452847
		 -1.36100721 0.27903223 0.82912385 -0.095674276 0.23170233 0.58104205 -0.33968568 0.33170938 -0.68261027
		 -1.46290851 0.72330904 0.22461873 -1.50311732 -0.065237522 0.16997671 -1.7143929 0.33769035 0.24266213
		 -0.26877213 -0.065109253 0.45365143 -0.19757557 0.67597914 -0.023463011 -0.23778439 -0.11256742 -0.078105092
		 0.013700247 0.27305079 -0.096148491 -1.092174292 0.62788868 -0.84385288 -1.11872995 0.10709858 -0.87994087
		 -0.53805697 0.56364155 -0.32445085 -0.015894413 0.57551575 -0.8800441 -0.04245019 0.054725647 -0.91613209
		 -0.57001185 0.63976288 -1.39944613 -0.59656751 0.11897278 -1.43553412 -0.5874927 0.38937998 -1.55888891
		 -1.24701953 0.3743825 -0.85713673 -0.54713154 0.29323435 -0.2010963 0.11239505 0.30823183 -0.90284848
		 -0.90048528 0.71077824 -1.18522191 -0.93402696 0.052984238 -1.23080349 -1.045238376 0.39671993 -1.32797694
		 -0.56461263 0.042851448 -0.36053884 -0.20059752 0.62963009 -0.52918148 -0.23413908 -0.02816391 -0.57476306
		 -0.089385986 0.28589439 -0.43200827;
	setAttr -s 192 ".ed";
	setAttr ".ed[0:165]"  0 8 1 8 1 1 3 10 1 10 4 1 1 14 1 4 6 1 6 12 1 12 1 1
		 7 5 1 6 7 1 8 9 1 14 9 1 0 2 1 9 2 1 17 15 1 9 17 1 16 17 1 10 17 1 5 3 1 7 10 1
		 5 11 1 13 11 1 7 13 1 12 13 1 8 13 1 0 11 1 12 16 1 4 16 1 16 14 1 11 15 1 2 15 1
		 15 3 1 18 26 1 26 19 1 21 28 1 28 22 1 19 32 1 22 24 1 24 30 1 30 19 1 25 23 1 24 25 1
		 26 27 1 32 27 1 18 20 1 27 20 1 35 33 1 27 35 1 34 35 1 28 35 1 23 21 1 25 28 1 23 29 1
		 31 29 1 25 31 1 30 31 1 26 31 1 18 29 1 30 34 1 22 34 1 34 32 1 29 33 1 20 33 1 33 21 1
		 36 44 1 44 37 1 39 46 1 46 40 1 37 50 1 40 42 1 42 48 1 48 37 1 43 41 1 42 43 1 44 45 1
		 50 45 1 36 38 1 45 38 1 53 51 1 45 53 1 52 53 1 46 53 1 41 39 1 43 46 1 41 47 1 49 47 1
		 43 49 1 48 49 1 44 49 1 36 47 1 48 52 1 40 52 1 52 50 1 47 51 1 38 51 1 51 39 1 54 62 1
		 62 55 1 57 64 1 64 58 1 55 68 1 58 60 1 60 66 1 66 55 1 61 59 1 60 61 1 62 63 1 68 63 1
		 54 56 1 63 56 1 71 69 1 63 71 1 70 71 1 64 71 1 59 57 1 61 64 1 59 65 1 67 65 1 61 67 1
		 66 67 1 62 67 1 54 65 1 66 70 1 58 70 1 70 68 1 65 69 1 56 69 1 69 57 1 72 80 1 80 73 1
		 75 82 1 82 76 1 73 86 1 76 78 1 78 84 1 84 73 1 79 77 1 78 79 1 80 81 1 86 81 1 72 74 1
		 81 74 1 89 87 1 81 89 1 88 89 1 82 89 1 77 75 1 79 82 1 77 83 1 85 83 1 79 85 1 84 85 1
		 80 85 1 72 83 1 84 88 1 76 88 1 88 86 1 83 87 1 74 87 1 87 75 1 90 98 1 98 91 1 93 100 1
		 100 94 1 91 104 1 94 96 1;
	setAttr ".ed[166:191]" 96 102 1 102 91 1 97 95 1 96 97 1 98 99 1 104 99 1 90 92 1
		 99 92 1 107 105 1 99 107 1 106 107 1 100 107 1 95 93 1 97 100 1 95 101 1 103 101 1
		 97 103 1 102 103 1 98 103 1 90 101 1 102 106 1 94 106 1 106 104 1 101 105 1 92 105 1
		 105 93 1;
	setAttr -s 96 -ch 384 ".fc[0:95]" -type "polyFaces" 
		f 4 1 4 11 -11
		mu 0 4 15 2 19 17
		f 4 0 10 13 -13
		mu 0 4 0 15 17 4
		f 4 -14 15 14 -31
		mu 0 4 4 17 23 20
		f 4 -12 -29 16 -16
		mu 0 4 17 19 21 23
		f 4 -17 -28 -4 17
		mu 0 4 23 21 7 18
		f 4 -15 -18 -3 -32
		mu 0 4 20 23 18 5
		f 4 2 -20 8 18
		mu 0 4 5 18 14 9
		f 4 3 5 9 19
		mu 0 4 18 7 12 14
		f 4 -9 22 21 -21
		mu 0 4 9 14 28 24
		f 4 -10 6 23 -23
		mu 0 4 14 12 26 28
		f 4 -24 7 -2 24
		mu 0 4 28 26 3 16
		f 4 -22 -25 -1 25
		mu 0 4 24 28 16 1
		f 4 -7 -6 27 -27
		mu 0 4 27 13 8 22
		f 4 -8 26 28 -5
		mu 0 4 2 27 22 19
		f 4 -26 12 30 -30
		mu 0 4 25 0 4 11
		f 4 20 29 31 -19
		mu 0 4 10 25 11 6
		f 4 33 36 43 -43
		mu 0 4 29 30 31 32
		f 4 32 42 45 -45
		mu 0 4 33 29 32 34
		f 4 -46 47 46 -63
		mu 0 4 34 32 35 36
		f 4 -44 -61 48 -48
		mu 0 4 32 31 37 35
		f 4 -49 -60 -36 49
		mu 0 4 35 37 38 39
		f 4 -47 -50 -35 -64
		mu 0 4 36 35 39 40
		f 4 34 -52 40 50
		mu 0 4 40 39 41 42
		f 4 35 37 41 51
		mu 0 4 39 38 43 41
		f 4 -41 54 53 -53
		mu 0 4 42 41 44 45
		f 4 -42 38 55 -55
		mu 0 4 41 43 46 44
		f 4 -56 39 -34 56
		mu 0 4 44 46 47 48
		f 4 -54 -57 -33 57
		mu 0 4 45 44 48 49
		f 4 -39 -38 59 -59
		mu 0 4 50 51 52 53
		f 4 -40 58 60 -37
		mu 0 4 30 50 53 31
		f 4 -58 44 62 -62
		mu 0 4 54 33 34 55
		f 4 52 61 63 -51
		mu 0 4 56 54 55 57
		f 4 65 68 75 -75
		mu 0 4 58 59 60 61
		f 4 64 74 77 -77
		mu 0 4 62 58 61 63
		f 4 -78 79 78 -95
		mu 0 4 63 61 64 65
		f 4 -76 -93 80 -80
		mu 0 4 61 60 66 64
		f 4 -81 -92 -68 81
		mu 0 4 64 66 67 68
		f 4 -79 -82 -67 -96
		mu 0 4 65 64 68 69
		f 4 66 -84 72 82
		mu 0 4 69 68 70 71
		f 4 67 69 73 83
		mu 0 4 68 67 72 70
		f 4 -73 86 85 -85
		mu 0 4 71 70 73 74
		f 4 -74 70 87 -87
		mu 0 4 70 72 75 73
		f 4 -88 71 -66 88
		mu 0 4 73 75 76 77
		f 4 -86 -89 -65 89
		mu 0 4 74 73 77 78
		f 4 -71 -70 91 -91
		mu 0 4 79 80 81 82
		f 4 -72 90 92 -69
		mu 0 4 59 79 82 60
		f 4 -90 76 94 -94
		mu 0 4 83 62 63 84
		f 4 84 93 95 -83
		mu 0 4 85 83 84 86
		f 4 97 100 107 -107
		mu 0 4 87 88 89 90
		f 4 96 106 109 -109
		mu 0 4 91 87 90 92
		f 4 -110 111 110 -127
		mu 0 4 92 90 93 94
		f 4 -108 -125 112 -112
		mu 0 4 90 89 95 93
		f 4 -113 -124 -100 113
		mu 0 4 93 95 96 97
		f 4 -111 -114 -99 -128
		mu 0 4 94 93 97 98
		f 4 98 -116 104 114
		mu 0 4 98 97 99 100
		f 4 99 101 105 115
		mu 0 4 97 96 101 99
		f 4 -105 118 117 -117
		mu 0 4 100 99 102 103
		f 4 -106 102 119 -119
		mu 0 4 99 101 104 102
		f 4 -120 103 -98 120
		mu 0 4 102 104 105 106
		f 4 -118 -121 -97 121
		mu 0 4 103 102 106 107
		f 4 -103 -102 123 -123
		mu 0 4 108 109 110 111
		f 4 -104 122 124 -101
		mu 0 4 88 108 111 89
		f 4 -122 108 126 -126
		mu 0 4 112 91 92 113
		f 4 116 125 127 -115
		mu 0 4 114 112 113 115
		f 4 129 132 139 -139
		mu 0 4 116 117 118 119
		f 4 128 138 141 -141
		mu 0 4 120 116 119 121
		f 4 -142 143 142 -159
		mu 0 4 121 119 122 123
		f 4 -140 -157 144 -144
		mu 0 4 119 118 124 122
		f 4 -145 -156 -132 145
		mu 0 4 122 124 125 126
		f 4 -143 -146 -131 -160
		mu 0 4 123 122 126 127
		f 4 130 -148 136 146
		mu 0 4 127 126 128 129
		f 4 131 133 137 147
		mu 0 4 126 125 130 128
		f 4 -137 150 149 -149
		mu 0 4 129 128 131 132
		f 4 -138 134 151 -151
		mu 0 4 128 130 133 131
		f 4 -152 135 -130 152
		mu 0 4 131 133 134 135
		f 4 -150 -153 -129 153
		mu 0 4 132 131 135 136
		f 4 -135 -134 155 -155
		mu 0 4 137 138 139 140
		f 4 -136 154 156 -133
		mu 0 4 117 137 140 118
		f 4 -154 140 158 -158
		mu 0 4 141 120 121 142
		f 4 148 157 159 -147
		mu 0 4 143 141 142 144
		f 4 161 164 171 -171
		mu 0 4 145 146 147 148
		f 4 160 170 173 -173
		mu 0 4 149 145 148 150
		f 4 -174 175 174 -191
		mu 0 4 150 148 151 152
		f 4 -172 -189 176 -176
		mu 0 4 148 147 153 151
		f 4 -177 -188 -164 177
		mu 0 4 151 153 154 155
		f 4 -175 -178 -163 -192
		mu 0 4 152 151 155 156
		f 4 162 -180 168 178
		mu 0 4 156 155 157 158
		f 4 163 165 169 179
		mu 0 4 155 154 159 157
		f 4 -169 182 181 -181
		mu 0 4 158 157 160 161
		f 4 -170 166 183 -183
		mu 0 4 157 159 162 160
		f 4 -184 167 -162 184
		mu 0 4 160 162 163 164
		f 4 -182 -185 -161 185
		mu 0 4 161 160 164 165
		f 4 -167 -166 187 -187
		mu 0 4 166 167 168 169
		f 4 -168 186 188 -165
		mu 0 4 146 166 169 147
		f 4 -186 172 190 -190
		mu 0 4 170 149 150 171
		f 4 180 189 191 -179
		mu 0 4 172 170 171 173;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "17EF01CF-404A-DA01-31CE-55B8F84A3D2F";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "5133D094-47FE-20A4-D1FD-848B0DDBFF00";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "8DEBEC80-4B16-FFA4-0659-318EB306D36E";
createNode displayLayerManager -n "layerManager";
	rename -uid "14DB699B-4559-8639-9C17-208D235A9B0A";
createNode displayLayer -n "defaultLayer";
	rename -uid "86AE5C5E-40F2-3B66-F326-6399803230D7";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "B173AEA7-4F92-ED58-32A5-70A43BE82A8E";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "3D71C6B6-43DD-F23E-620E-1993A6646213";
	setAttr ".g" yes;
createNode groupId -n "groupId14";
	rename -uid "18234456-4791-1D55-DE86-05B3CAF84D0D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "954D1DF4-43C0-9161-50FA-D6B6F9856561";
	setAttr ".ihi" 0;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "F318E294-4E79-DCF2-9710-92B2F41DC1C2";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1419\n            -height 525\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"0\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 1\n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n"
		+ "                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n"
		+ "                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n"
		+ "                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n"
		+ "\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n"
		+ "                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1419\\n    -height 525\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1419\\n    -height 525\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "122F4E70-4352-E609-4990-529DDA0D223F";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".wsn" -type "string" "ACEScg";
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "groupId15.id" "pSphere7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pSphere7Shape.iog.og[0].gco";
connectAttr "groupId14.id" "pCube7Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube7Shape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCube7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pSphere7Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId14.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId15.msg" ":initialShadingGroup.gn" -na;
// End of SpikyCrystal.ma
